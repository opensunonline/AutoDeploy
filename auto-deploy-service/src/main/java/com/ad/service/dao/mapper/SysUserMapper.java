package com.ad.service.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.ad.service.dao.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
