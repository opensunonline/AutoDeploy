package com.ad.service.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ad.service.dao.model.SysUser;
import com.ad.service.services.SysUserService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@RestController
@RequestMapping("/sysUser")
public class SysUserController {

	@Autowired
	private SysUserService sysUserService;

	@GetMapping(value = "test")
	public void test(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE,HEAD");
		response.addHeader("Access-Control-Allow-Headers", "S_ID,content-type");
		response.addHeader("Access-Control-Allow-Headers",
				"Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		response.addHeader("Access-Control-Max-Age", "3600000");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		// 让请求，不被缓存，
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Cache-Control", "no-cors");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		// 测试逻辑删除功能
		SysUser sysUser = sysUserService.selectById(1);
		// 测试分页
		IPage<SysUser> sysUserIPage = sysUserService.selectPage(new Page<SysUser>(1, 10), new QueryWrapper<>());

		// 测试公共字段自动填充
		SysUser sysUser1 = new SysUser();
		sysUser1.setUsername("shen");
		sysUser1.setNickname("shen");
		sysUser1.setPassword("shen");
		sysUserService.insert(sysUser1);

		// 测试乐观锁
		SysUser sysUser2 = new SysUser();
		sysUser2.setId(19);
		sysUser2.setUsername("shen2");
		sysUser2.setNickname("shen2");
		sysUser2.setPassword("shen2");
		sysUser2.setUpdateVersion(1);
		sysUserService.updateById(sysUser2);
		JSONObject object = new JSONObject();
		object.put("ss", "ssssssss");
		try {
			response.getWriter().write(object.toJSONString());
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
