package com.ad.service.services;

import com.ad.service.dao.model.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysUserService extends IService<SysUser> {

}
