package com.ad.service.services;

import org.springframework.stereotype.Service;

import com.ad.service.dao.mapper.SysUserMapper;
import com.ad.service.dao.model.SysUser;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
