import Vue from "vue";
import {InputNumber ,Radio, LocaleProvider, Transfer, Row, Col, Button, Input, Card, Avatar, Form, Select, Layout, Menu, Icon, Breadcrumb, Skeleton, Popconfirm, notification } from 'ant-design-vue';
// import {Header, Sider, Content, Footer} from 'ant-design-vue/lib/layout';
import App from "./App";
// import 'ant-design-vue/dist/antd.css'

// Vue.component(Button.name, Button);
// Vue.component(Layout.name, Layout);
// Vue.component(B)
// Vue.component(Menu.name, Menu);
Vue.use(InputNumber)
Vue.use(Radio)
Vue.use(LocaleProvider)
Vue.use(Transfer)
Vue.use(Row)
Vue.use(Col)
Vue.use(Button)
Vue.use(Input)
Vue.use(Card)
Vue.use(Avatar)
Vue.use(Form)
Vue.use(Select)
Vue.use(Layout)
Vue.use(Menu)
Vue.use(Icon)
Vue.use(Breadcrumb)
Vue.use(Skeleton)
Vue.use(Popconfirm)
Vue.prototype.$notification = notification
Vue.config.productionTip = false;

new Vue({
    render: h => h(App)
}).$mount("#app");